﻿using System.Text.Json;
using Dawn;

namespace Rocket;

internal class Program {
    private static void Main(string[] args) {
        if (args.Length < 2) {
            Console.WriteLine("Usage: Rocket path/to/game/folder path/to/extract/folder");
            return;
        }

        using var writer = new StreamWriter(Path.Combine(args[1], "filelist.txt"));
        foreach (var headerlibPath in Directory.EnumerateFiles(args[0], "*.pc_headerlib", new EnumerationOptions { RecurseSubdirectories = true, ReturnSpecialDirectories = false })) {
            var headerlib = new HeaderLib(headerlibPath);
            if (headerlib.Name != null) {
                Console.WriteLine(headerlib.Name.Path);
                var dest = Path.Combine(args[1], headerlib.Name.StrippedName);
                var dir = Path.GetDirectoryName(dest);
                if (!Directory.Exists(dir)) {
                    Directory.CreateDirectory(dir!);
                }

                File.WriteAllText(dest + ".json", JsonSerializer.Serialize(headerlib, new JsonSerializerOptions { WriteIndented = true, Converters = { new MemoryConverterFactory() } }));
            }

            foreach (var (name, resourceLib, resource) in headerlib.GetResources()) {
                var dest = Path.Combine(args[1], name.StrippedName);
                var dir = Path.GetDirectoryName(dest);
                if (!Directory.Exists(dir)) {
                    Directory.CreateDirectory(dir!);
                }

                if (File.Exists(dest)) {
                    continue;
                }

                writer.WriteLine(name.Path + " " + name.Hash);

                File.WriteAllBytes(dest, resource.ToArray());
                Console.WriteLine(name.Path);
            }
        }
    }
}
