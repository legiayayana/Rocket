﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace Rocket;

public class MemoryConverterFactory : JsonConverterFactory {
    public override bool CanConvert(Type typeToConvert) => typeToConvert.IsConstructedGenericType && typeToConvert.GetGenericTypeDefinition() == typeof(Memory<>);

    public override JsonConverter CreateConverter(Type typeToConvert, JsonSerializerOptions options) {
        var type = typeof(MemoryConverter<>).MakeGenericType(typeToConvert.GenericTypeArguments[0]);
        return (JsonConverter) Activator.CreateInstance(type)!;
    }

    public class MemoryConverter<T> : JsonConverter<Memory<T>> {
        public override Memory<T> Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
            var arr = JsonSerializer.Deserialize<T[]>(ref reader, options);
            return arr.AsMemory();
        }

        public override void Write(Utf8JsonWriter writer, Memory<T> value, JsonSerializerOptions options) {
            JsonSerializer.Serialize(writer, value.ToArray(), options);
        }
    }
}
