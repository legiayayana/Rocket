﻿using System.Diagnostics;
using Dawn.Structs;
using Dawn.Structs.HeaderLib;
using Microsoft.Toolkit.HighPerformance;
using Microsoft.Toolkit.HighPerformance.Buffers;

namespace Dawn;

public class HeaderLib {
    public HeaderLib(string path) {
        RootDir = Path.GetDirectoryName(path)!;

        using var stream = File.OpenRead(path);
        Header = stream.Read<HeaderLibraryHeader>();
        if (Header.Magic != HeaderLibraryHeader.HEADER_MAGIC) {
            throw new Exception("Invalid header magic");
        }

        if (Header.ReferenceChunkSize > 0) {
            using var pooled = MemoryOwner<byte>.Allocate(Header.ReferenceChunkSize);
            stream.ReadExactly(pooled.Span);
            using var reader = new MemoryReader(pooled);
            var count = reader.Read<int>();
            References.EnsureCapacity(count);
            var resourceId = reader.ReadArray<uint>(count);
            var flags = reader.ReadArray<uint>(count);
            var referenceOffsets = reader.ReadArray<int>(count);
            using var stringBuffer = new MemoryReader(reader.Partition(reader.Read<int>()));
            for (var i = 0; i < count; i++) {
                stringBuffer.Position = referenceOffsets[i];
                References.Add(new HeaderLibraryReference(resourceId[i], flags[i], stringBuffer.ReadCString()));
            }
        }

        if (Header.DataChunkSize > 0) {
            using var pooled = MemoryOwner<byte>.Allocate(Header.DataChunkSize);
            stream.ReadExactly(pooled.Span);
            using var reader = new MemoryReader(pooled);
            States = new BIN1(reader);
            using var statesReader = States.CreateReader();
            Resources = new HeaderLibraryResources(statesReader);

            if (Resources.Resources?.Length > 0) {
                var name = (Resources.Resources[0].Resource1 ?? Resources.Resources[0].Resource2)?.Path;
                if (!string.IsNullOrEmpty(name) && name.Contains(']')) {
                    Name = new AssemblyResource(name[..(name.LastIndexOf(']') + 1)] + ".pc_headerlib");
                }
            }
        }

        Debug.Assert(Header.Unknown1 == 0, "Header.Unknown1 == 0");
    }

    public AssemblyResource? Name { get; set; }
    public string RootDir { get; set; }
    public HeaderLibraryHeader Header { get; }
    public List<HeaderLibraryReference> References { get; } = new();
    public BIN1? States { get; }
    public HeaderLibraryResources? Resources { get; }

    public void Validate() {
        if (Resources?.Resources == null) {
            return;
        }

        Debug.Assert(Resources!.ResourceIds.Length == Resources!.Resources!.Sum(x => x.ResourceHeaders.Length), "Resources.ResourceIds.Length == Resources.Resources.Sum(x => x.ResourceHeaders.Length)");
    }

    public IEnumerable<(AssemblyResource ResourceId, AssemblyResource? ResourceLib, Memory<byte> Data)> GetResources() {
        if (Resources?.Resources == null) {
            yield break;
        }

        var resourceId = 0;

        foreach (var resource in Resources.Resources) {
            // these streams can be gigantic, so we'll just read the resource into memory rather than the entire file.
            using var resourceStream = File.OpenRead(Path.Combine(RootDir, resource.Resource1!.Hash + ".pc_resourcelib"));
            var localResourceId = 0;
            for (var resourceHeaderIndex = 0; resourceHeaderIndex < resource.ResourceHeaders.Span.Length; resourceHeaderIndex++) {
                var header = resource.ResourceHeaders.Span[resourceHeaderIndex];
                var offset = resource.Offsets.Span[resourceHeaderIndex];
                resourceStream.Position = offset.Offset;
                var buffer = new byte[header.Length + offset.Length].AsMemory();
                header.CopyTo(buffer);
                resourceStream.ReadExactly(buffer.Span[header.Length..]);
                var name = Resources.ResourceIds[resourceId++];
                Debug.Assert((name.ResourceId & 0xFFFFFF) == localResourceId++, "(name.ResourceId & 0xFFFFFF) == localResourceId");
                yield return (name.Name!, resource.Resource1, buffer);
            }
        }

        // process "meta" files, these are auxiliary files that are not part of the resource library
        // todo: check if this is only pc_scenemetadata, pc_wem exists but idk what uses that.
        if (Resources.Meta != null) {
            foreach(var meta in Resources.Meta) {
                if (meta == null) {
                    continue;
                }

                var metaFile = Path.Combine(RootDir, meta.Hash + Path.GetExtension(meta.Path));
                if (!File.Exists(metaFile)) {
                    continue;
                }

                yield return (meta, null, File.ReadAllBytes(metaFile));
            }
        }
    }
}
