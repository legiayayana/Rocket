﻿using System.Security.Cryptography;
using System.Text;

namespace Dawn;

public static class HashLib {
    private static char HexDigit(int n) {
        if (n < 10) {
            return (char) (n + '0');
        }

        return (char) (n - 10 + 'A');
    }

    public static string ConvertPathToAssemblyHash(string path) {
        var hashBytes = MD5.HashData(Encoding.ASCII.GetBytes(path.ToLower()[..(path.LastIndexOf('.') + 1)]));
        Span<char> stack = stackalloc char[32];

        for (var index = 0; index < hashBytes.Length; index++) {
            var b = hashBytes[index];
            stack[index * 2] = HexDigit((b >> 4) & 0xF);
            stack[index * 2 + 1] = HexDigit(b & 0xF);
        }

        return new string(stack);
    }
}
