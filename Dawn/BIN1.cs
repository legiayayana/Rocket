﻿using System.Buffers.Binary;
using Dawn.Structs;
using Dawn.Structs.BinaryData;

namespace Dawn;

public class BIN1 {
    public BIN1(MemoryReader data) {
        Header = data.Read<BinaryHeader>();
        if (Header.Magic != BinaryHeader.HEADER_MAGIC) {
            throw new Exception("Invalid header magic");
        }

        data.Align();
        Header = Header with { DataSize = BinaryPrimitives.ReverseEndianness(Header.DataSize) };
        Data = data.PartitionMemory(Header.DataSize);
        for (var i = 0; i < Header.SectionCount; ++i) {
            var (typeId, size) = data.Read<BinarySectionHeader>();
            Sections.Add(new BinarySection(typeId, data.PartitionMemory(size)));
        }
    }

    public BinaryHeader Header { get; }
    public Memory<byte> Data { get; set; }
    public List<BinarySection> Sections { get; } = new();

    public MemoryReader CreateReader() {
        var reader = new MemoryReader(Data);
        reader.Alignment = Header.Alignment;
        return reader;
    }
}
