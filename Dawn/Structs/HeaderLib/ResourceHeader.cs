﻿namespace Dawn.Structs.HeaderLib;

public record struct ResourceHeader(uint Magic, int EmbeddedSize, long Length, int UnknownSize, int ChunkSize);
