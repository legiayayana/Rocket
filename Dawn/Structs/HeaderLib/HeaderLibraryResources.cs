﻿namespace Dawn.Structs.HeaderLib;

public record HeaderLibraryResources {
    public HeaderLibraryResources(MemoryReader reader) {
        Localization = reader.ReadZString();
        StreamGroups = reader.ReadZString2D();
        Resources = reader.ReadTArrayImpl<HeaderLibraryResource>();
        Meta = reader.ReadZAssemblyResource2D();
        Id = reader.Read<Guid>();
        var resourceIds = reader.ReadTArray<byte>();
        if (resourceIds.Length > 0) {
            using var resourceReader = new MemoryReader(resourceIds);
            var count = resourceReader.Read<int>();
            ResourceIds = new HeaderLibraryResourceId[count];
            for (var i = 0; i < count; ++i) {
                ResourceIds[i] = resourceReader.ReadClass<HeaderLibraryResourceId>();
            }
        } else {
            ResourceIds = Array.Empty<HeaderLibraryResourceId>();
        }

        Unknown3 = reader.ReadTArray<int>();
    }

    public string? Localization { get; set; }
    public string?[]? StreamGroups { get; set; }
    public HeaderLibraryResource[]? Resources { get; set; }
    public AssemblyResource?[]? Meta { get; set; }
    public Guid Id { get; set; }
    public HeaderLibraryResourceId[] ResourceIds { get; set; }
    public Memory<int> Unknown3 { get; set; }
}
