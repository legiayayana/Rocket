﻿using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Dawn.Structs.HeaderLib;

public record HeaderLibraryResource {
    public HeaderLibraryResource() { }

    public HeaderLibraryResource(MemoryReader reader, int flags) {
        Resource1 = reader.ReadZAssemblyResource();
        Resource2 = reader.ReadZAssemblyResource();
        Unknown1 = reader.Read<ulong>();
        Unknown2 = reader.Read<ulong>();
        Unknown3 = reader.Read<ulong>();
        Unknown4 = reader.ReadZString2D();
        Unknown5 = reader.Read<ulong>();
        Unknown6 = reader.ReadTArray<int>();
        Unknown7 = reader.Read<ulong>();
        Unknown8 = reader.Read<ulong>();
        ResourceLibraryHeader = reader.ReadTArray<byte>();
        ResourceHeaders = reader.ReadTArray2D<byte>();

        Offsets = new ResourceOffset[ResourceHeaders.Length].AsMemory();
        var offset = 24L; // skip RLIB header.
        for (var i = 0; i < ResourceHeaders.Length; ++i) {
            var header = MemoryMarshal.Read<ResourceHeader>(ResourceHeaders.Span[i].Span);
            Debug.Assert(header.Length < uint.MaxValue, "header.Length < uint.MaxValue");
            Offsets.Span[i] = new ResourceOffset(offset, (int) header.Length);
            offset += header.Length;
        }

        Unknown9 = reader.ReadTArray<byte>(); // always empty??
        UnknownA = reader.ReadTArray<byte>(); // always empty??
        UnknownB = reader.ReadTArray<int>();
        UnknownC = reader.ReadTArray<byte>(); // some complicated struct with integers.
        // unkc = TArray<struct>; int; int;
        // struct = int; int; int; int; OR int; int[int];

        Debug.Assert(Unknown9.IsEmpty, "Unknown9.IsEmpty");
        Debug.Assert(UnknownA.IsEmpty, "UnknownA.IsEmpty");
    }

    public AssemblyResource? Resource1 { get; set; }
    public AssemblyResource? Resource2 { get; set; }
    public ulong Unknown1 { get; set; }
    public ulong Unknown2 { get; set; }
    public ulong Unknown3 { get; set; }
    public string?[]? Unknown4 { get; set; }
    public ulong Unknown5 { get; set; }
    public Memory<int> Unknown6 { get; set; }
    public ulong Unknown7 { get; set; }
    public ulong Unknown8 { get; set; }
    public Memory<byte> ResourceLibraryHeader { get; set; }
    public Memory<Memory<byte>> ResourceHeaders { get; set; }
    public Memory<ResourceOffset> Offsets { get; set; }
    public Memory<byte> Unknown9 { get; set; }
    public Memory<byte> UnknownA { get; set; }
    public Memory<int> UnknownB { get; set; }
    public Memory<byte> UnknownC { get; set; }
}
