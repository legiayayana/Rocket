﻿using System.Diagnostics;

namespace Dawn.Structs.HeaderLib;

public record HeaderLibraryResourceId {
    public HeaderLibraryResourceId() => Name = "";

    public HeaderLibraryResourceId(MemoryReader reader) {
        ResourceOwnerId = reader.Read<uint>();
        ResourceId = reader.Read<uint>();
        Flags = reader.Read<ulong>();
        Name = reader.ReadString();
        Unknown1 = reader.Read<uint>();
        Unknown2 = reader.Read<ushort>();
        Debug.Assert(Unknown1 == 0, "Unknown1 == 0");
        Debug.Assert(Unknown2 == 0, "Unknown2 == 0");
    }

    public uint ResourceOwnerId { get; set; }
    public uint ResourceId { get; set; }
    public ulong Flags { get; set; }
    public AssemblyResource? Name { get; set; }
    public uint Unknown1 { get; set; }
    public ushort Unknown2 { get; set; }
}
