﻿namespace Dawn.Structs.HeaderLib;

public record HeaderLibraryReference(uint ResourceId, uint Flags, AssemblyResource Path);
