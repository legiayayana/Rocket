﻿namespace Dawn.Structs.HeaderLib;

public record struct ResourceOffset(long Offset, int Length);
