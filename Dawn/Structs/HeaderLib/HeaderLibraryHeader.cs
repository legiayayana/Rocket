﻿using System.Runtime.InteropServices;

namespace Dawn.Structs.HeaderLib;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public record struct HeaderLibraryHeader {
    public const uint HEADER_MAGIC = 0x484C4942; // 'HLIB'
    public uint Magic { get; set; }
    public int ReferenceChunkSize { get; set; }
    public int DataChunkSize { get; set; }
    public int Unknown1 { get; set; }
    public int SystemMemoryRequirement { get; set; }
    public int VideoMemoryRequirement { get; set; }
}
