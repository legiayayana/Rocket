﻿namespace Dawn.Structs;

public record AssemblyResource(string Path) {
    public string Hash => HashLib.ConvertPathToAssemblyHash(Path);

    // the name is a series of nested components within larger files.
    // it's technically possible to rebuild these files completely but it requires more advanced parsing of the paths and how it's represented by the game in memory.
    // format is:
    // [module:/path/to/module.class].pc_type
    // [assembly:/path/to/asset.class].pc_type
    // [assembly:/path/to/asset.class]([modules:/path/to/class/instance.class].type,[modules:/path/to/class/instance.class].type,...).pc_type
    // we typically don't care about the class type but we do care about everything else.
    public string StrippedName {
        get {
            var parts = Path.Split(':', 2);
            var type = parts[0].Replace("[", "");
            var path = parts[1].Replace("assembly:", "");
            path = path.Replace("modules:", "");
            path = path.Replace("project:", "");
            path = path.Replace("[", "");
            path = path.Replace("]", "");
            path = path.Replace("?", "");
            var index = path.LastIndexOf('(');
            if (index > -1) {
                path = path[..index];
            }

            while (path.Contains(':')) {
                index = path.LastIndexOf('(');
                if (index > -1) {
                    path = path[..index];
                    continue;
                }

                path = path.Replace(":", "");
            }

            index = path.LastIndexOf('.');
            if (index > -1) {
                path = path[..index];
            }

            var ext = Path.Split('.')[^1];

            return $"{type}{path}.{Hash}.{ext}";
        }
    }

    public static implicit operator AssemblyResource(string path) => new(path);

    public static AssemblyResource FromString(string path) => new AssemblyResource(path);
}
