﻿using System.Runtime.InteropServices;

namespace Dawn.Structs.Graphics;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public record struct TextureHeader {
    public const uint RESOURCE_MAGIC = 0x54455854; // 'TEXT'
    public const uint HEADER_MAGIC = 0x54455848; // 'TEXH'

    public uint Magic { get; set; }
    public uint Version { get; set; }
    public int Length { get; set; }
    public ushort Width { get; set; }
    public ushort Height { get; set; }
    public ushort Depth { get; set; }
    public byte Mips { get; set; }
    public TextureFormat Format { get; set; }
    public ushort Unknown1 { get; set; }
    public ushort Unknown2 { get; set; }

    public DXGI_FORMAT DXGIFormat =>
        Format switch {
            TextureFormat.RGBA32_FLOAT => DXGI_FORMAT.R32G32B32A32_FLOAT,
            TextureFormat.RGBA16_FLOAT => DXGI_FORMAT.R16G16B16A16_FLOAT,
            TextureFormat.RGBA8 => DXGI_FORMAT.R8G8B8A8_UNORM,
            TextureFormat.RG16 => DXGI_FORMAT.R16G16_UNORM,
            TextureFormat.R16_FLOAT => DXGI_FORMAT.R16_FLOAT,
            TextureFormat.R16 => DXGI_FORMAT.R16_UNORM,
            TextureFormat.BC1 => DXGI_FORMAT.BC1_UNORM,
            TextureFormat.BC1_SRGB => DXGI_FORMAT.BC1_UNORM_SRGB,
            TextureFormat.BC2 => DXGI_FORMAT.BC2_UNORM,
            TextureFormat.BC2_SRGB => DXGI_FORMAT.BC2_UNORM_SRGB,
            TextureFormat.BC3 => DXGI_FORMAT.BC3_UNORM,
            TextureFormat.BC4 => DXGI_FORMAT.BC4_UNORM,
            TextureFormat.BC5 => DXGI_FORMAT.BC5_UNORM,
            TextureFormat.BC6 => DXGI_FORMAT.BC6H_UF16,
            TextureFormat.BC7 => DXGI_FORMAT.BC7_UNORM,
            TextureFormat.BC7_SRGB => DXGI_FORMAT.BC7_UNORM_SRGB,
            _ => DXGI_FORMAT.UNKNOWN
        };
}
