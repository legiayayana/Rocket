﻿namespace Dawn.Structs.Graphics;

// 99% like DXGI, but it drifts towards the end.
public enum TextureFormat : byte {
    RGBA32_FLOAT = 0x02,
    RGBA16_FLOAT = 0x0A,
    RGBA8 = 0x1C,
    RG16 = 0x24,
    R16_FLOAT = 0x3C,
    R16 = 0x3D,
    BC1 = 0x4A,
    BC1_SRGB = 0x4B,
    BC2 = 0x4C,
    BC2_SRGB = 0x4D,
    BC3 = 0x51,
    BC3_SRGB = 0x51,
    BC4 = 0x53,
    BC5 = 0x56,
    BC6 = 0x58,
    BC7 = 0x5B,
    BC7_SRGB = 0x5C,
}
