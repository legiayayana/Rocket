﻿namespace Dawn.Structs.BinaryData;

public record BinarySection(uint TypeId, Memory<byte> Data);
