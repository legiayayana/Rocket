﻿using System.Runtime.InteropServices;

namespace Dawn.Structs.BinaryData;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public record struct BinaryHeader {
    public const uint HEADER_MAGIC = 0x314E4942; // 'BIN1'
    public uint Magic { get; set; }
    public bool IsBigEndian { get; set; }
    public byte Alignment { get; set; }
    public ushort SectionCount { get; set; }
    public int DataSize { get; set; }
}
