﻿namespace Dawn.Structs.BinaryData;

public record struct BinarySectionHeader(uint TypeId, int Size) {
    public const uint REBASE_TYPEID = 0x12EBA5ED;
}