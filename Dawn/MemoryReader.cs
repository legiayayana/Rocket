﻿using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Dawn.Structs;
using Microsoft.Toolkit.HighPerformance.Buffers;

namespace Dawn;

public sealed class MemoryReader : IDisposable {
    public MemoryReader(MemoryOwner<byte> data) => Data = data;

    public MemoryReader(Memory<byte> data) {
        Data = MemoryOwner<byte>.Allocate(data.Length);
        data.CopyTo(Data.Memory);
    }

    public MemoryOwner<byte> Data { get; }
    public int Position { get; set; }
    public int Alignment { get; set; } = 8;
    public int Length => Data.Length;
    public int Remaining => Data.Length - Position;

    public void Dispose() {
        Data.Dispose();
    }

    public T Read<T>() where T : struct {
        var value = MemoryMarshal.Read<T>(Data.Span[Position..]);
        Position += Unsafe.SizeOf<T>();
        return value;
    }

    public T ReadClass<T>(params object?[] extra) where T : class, new() {
        var args = new object?[extra.Length + 1];
        args[0] = this;
        extra.CopyTo(args, 1);
        return (Activator.CreateInstance(typeof(T), args) as T)!;
    }

    public bool ReadBoolean() {
        var value = Read<uint>();
        if (value > 1) {
            throw new InvalidDataException($"Expected 0 or 1 for a boolean value, got {value}");
        }

        return value == 1;
    }

    public bool ReadBit() {
        var value = Read<byte>();
        if (value > 1) {
            throw new InvalidDataException($"Expected 0 or 1 for a bit value, got {value}");
        }

        return value == 1;
    }

    public string ReadString(int? count = null) {
        count ??= Read<int>();
        var value = Encoding.UTF8.GetString(Data.Span.Slice(Position, count.Value));
        Position += count.Value;
        return value;
    }

    public string ReadCString() {
        var count = Data.Span[Position..].IndexOf((byte) 0);
        if (count == -1) {
            count = Data.Length;
        }

        var value = Encoding.UTF8.GetString(Data.Span.Slice(Position, count));
        Position += count;
        return value;
    }

    public Memory<T> ReadTArray<T>() where T : struct {
        var offset = Read<long>();
        var flags = Read<int>();
        var size = Read<int>();
        Debug.Assert(flags == size, "flags == size");
        if (offset < 0 || size == 0) {
            return Memory<T>.Empty;
        }

        var value = new T[size].AsMemory();
        Data.Span.Slice((int) offset, size).CopyTo(MemoryMarshal.AsBytes(value.Span));
        return value;
    }

    public Memory<Memory<T>> ReadTArray2D<T>() where T : struct {
        var offset = Read<long>();
        var flags = Read<int>();
        var size = Read<int>();
        Debug.Assert(flags == size, "flags == size");
        if (offset < 0 || size == 0) {
            return Memory<Memory<T>>.Empty;
        }

        var value = new Memory<T>[size];
        var position = Position;
        Position = (int) offset;
        for (var i = 0; i < value.Length; ++i) {
            value[i] = ReadTArray<T>();
        }

        Position = position;
        return value.AsMemory();
    }

    public string? ReadZString() {
        var offset = Read<long>();
        var flags = Read<int>();
        var size = Read<int>();
        if (offset < 0 || size == 0) {
            return null;
        }

        var value = new byte[size].AsMemory();
        Data.Span.Slice((int) offset, size).CopyTo(MemoryMarshal.AsBytes(value.Span));
        var length = flags & 0x3fffffff;
        var charset = flags >> 30;
        Debug.Assert(charset == 1, "charset == 1");
        Debug.Assert(length < 1024, "length < 1024");
        return Encoding.UTF8.GetString(value[..length].Span);
    }

    public string?[]? ReadZString2D() {
        var offset = Read<long>();
        var flags = Read<int>();
        var size = Read<int>();
        Debug.Assert(flags == size, "flags == size");
        if (offset < 0 || size == 0) {
            return null;
        }

        var value = new string?[size];
        var position = Position;
        Position = (int) offset;
        for (var i = 0; i < size; ++i) {
            value[i] = ReadZString();
        }

        Position = position;
        return value;
    }

    public AssemblyResource? ReadZAssemblyResource() {
        var offset = Read<long>();
        var flags = Read<int>();
        var size = Read<int>();
        if (offset < 0 || size == 0) {
            return null;
        }

        var value = new byte[size].AsMemory();
        Data.Span.Slice((int) offset, size).CopyTo(MemoryMarshal.AsBytes(value.Span));
        var length = flags & 0x3fffffff;
        var charset = flags >> 30;
        Debug.Assert(charset == 1, "charset == 1");
        Debug.Assert(length < 1024, "length < 1024");
        return new AssemblyResource(Encoding.UTF8.GetString(value[..length].Span));
    }

    public AssemblyResource?[]? ReadZAssemblyResource2D() {
        var offset = Read<long>();
        var flags = Read<int>();
        var size = Read<int>();
        Debug.Assert(flags == size, "flags == size");
        if (offset < 0 || size == 0) {
            return null;
        }

        var value = new AssemblyResource?[size];
        var position = Position;
        Position = (int) offset;
        for (var i = 0; i < size; ++i) {
            value[i] = ReadZAssemblyResource();
        }

        Position = position;
        return value;
    }

    public T[]? ReadTArrayImpl<T>(params object?[] extra) where T : class, new() {
        var offset = Read<long>();
        var flags = Read<int>();
        var size = Read<int>();
        Debug.Assert(flags == size, "flags == size");
        if (offset < 0 || size == 0) {
            return null;
        }

        var value = new T[size];
        var type = typeof(T);
        var args = new object?[extra.Length + 2];
        args[0] = this;
        args[1] = flags;
        extra.CopyTo(args, 1);
        var position = Position;
        Position = (int) offset;
        for (var i = 0; i < size; ++i) {
            value[i] = (T) Activator.CreateInstance(type, args)!;
            Align(Alignment);
        }

        Position = position;
        return value;
    }

    public T[]?[]? ReadTArrayImpl2D<T>() where T : class, new() {
        var offset = Read<long>();
        var flags = Read<int>(); // flags
        var size = Read<int>();
        Debug.Assert(flags == size, "flags == size");
        if (offset < 0 || size == 0) {
            return null;
        }

        var value = new T[]?[size];
        var position = Position;
        Position = (int) offset;
        for (var i = 0; i < value.Length; ++i) {
            value[i] = ReadTArrayImpl<T>();
        }

        Position = position;
        return value;
    }

    public MemoryOwner<byte> Partition(int pos, int size) {
        if (size == -1) {
            size = Data.Length - pos;
        }

        var allocator = MemoryOwner<byte>.Allocate(size);
        Data.Span.Slice(pos, size).CopyTo(allocator.Span);
        return allocator;
    }

    public MemoryOwner<byte> Partition(int count) {
        var pos = Position;
        Position += count;
        return Partition(pos, count);
    }

    public Memory<byte> PartitionMemory(int pos, int size) {
        if (size == -1) {
            size = Data.Length - pos;
        }

        var allocator = new byte[size].AsMemory();
        Data.Span.Slice(pos, size).CopyTo(allocator.Span);
        return allocator;
    }

    public Memory<byte> PartitionMemory(int count) {
        var pos = Position;
        Position += count;
        return PartitionMemory(pos, count);
    }

    public void Align(int v = 16) {
        Position = unchecked(Position + (v - 1)) & ~(v - 1);
    }

    public Span<T> ReadArray<T>(int count) where T : unmanaged {
        var size = Unsafe.SizeOf<T>() * count;
        var value = MemoryMarshal.Cast<byte, T>(Data.Span.Slice(Position, size));
        Position += size;
        return value;
    }
}
